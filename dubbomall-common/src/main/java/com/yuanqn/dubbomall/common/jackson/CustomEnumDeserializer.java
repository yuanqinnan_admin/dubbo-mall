package com.yuanqn.dubbomall.common.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.yuanqn.dubbomall.common.bean.BaseEnum;
import org.springframework.beans.BeanUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

/**
 * @description: 自定义枚举反序列化
 * @author: yuanqinnan
 * @create: 2021-04-24 22:39
 **/
public class CustomEnumDeserializer extends JsonDeserializer<BaseEnum> {
    @Override
    public BaseEnum deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        Class<BaseEnum> findPropertyType = (Class<BaseEnum>) BeanUtils.findPropertyType(jsonParser.currentName(), jsonParser.getCurrentValue().getClass());
        Optional<BaseEnum> any = Arrays.stream(findPropertyType.getEnumConstants()).filter(e -> e.getValue().equals(node.asInt())).findAny();
        return any.orElseGet(null);
    }
}
