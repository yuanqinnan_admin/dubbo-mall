package com.yuanqn.dubbomall.common.bean;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yuanqn.dubbomall.common.jackson.CustomEnumSerializer;

import java.io.Serializable;

/**
 * @description:
 * @author: yuanqinnan
 * @create: 2021-04-24 22:34
 **/
@JsonSerialize(using = CustomEnumSerializer.class)
public interface BaseEnum <T extends Serializable> extends IEnum<T> {
    /**
     * 枚举数据库存储值
     */
    @Override
    T getValue();

    /**
     * 展示文本
     */
    String getText();

    /**
     * 枚举名称
     */
    String getName();
}
