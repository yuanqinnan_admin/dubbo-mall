package com.yuanqn.dubbomall.common.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.yuanqn.dubbomall.common.bean.BaseEnum;

import java.io.IOException;

/**
 * @description: 自定义枚举序列化
 * @author: yuanqinnan
 * @create: 2021-04-24 22:37
 **/
public class CustomEnumSerializer extends JsonSerializer {

    @Override
    public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        BaseEnum baseEnum = (BaseEnum) o;
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("value", (Integer) baseEnum.getValue());
        jsonGenerator.writeStringField("text", baseEnum.getText());
        jsonGenerator.writeStringField("name", baseEnum.getName());
        jsonGenerator.writeEndObject();
    }
}
