package com.yuanqn.dubbomall.common.bean;

import java.io.Serializable;

/**
 * @description: 基础类
 * @author: yuanqinnan
 * @create: 2021-04-24 22:29
 **/
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
}
