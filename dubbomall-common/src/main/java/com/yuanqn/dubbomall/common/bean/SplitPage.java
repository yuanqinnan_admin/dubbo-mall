package com.yuanqn.dubbomall.common.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @description: 分页插件
 * @author: yuanqinnan
 * @create: 2021-04-24 23:00
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class SplitPage extends BaseEntity {

    /**
     * 当前页，默认1（从1开始）
     */
    @ApiModelProperty(value = "当前页，默认1（从1开始）", example = "1", required = true, position = 999)
    private Integer pageNum = 1;

    /**
     * 每页行数，默认10
     */
    @ApiModelProperty(value = "每页行数，默认10", example = "10", required = true, position = 1000)
    private Integer pageSize = 10;
}
