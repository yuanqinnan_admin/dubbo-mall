package com.yuanqn.dubbomall.common.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @description: 接口返回值
 * @author: yuanqinnan
 * @create: 2021-04-24 22:32
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ApiResult<T> extends BaseEntity{
}
