package com.yuanqn.dubbomall.common.exception;

import com.yuanqn.dubbomall.common.enums.ApiResultCodeEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @description: 请求异常类
 * @author: yuanqinnan
 * @create: 2021-04-24 22:51
 **/
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
public class ApiException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 响应状态回执码
     */
    protected long code;

    /**
     * 响应回执消息
     */
    protected String msg;

    /**
     * 异常信息
     */
    private Exception ex;

    public static ApiException argError(String msg) {
        return restException(ApiResultCodeEnum.ARG_ERROR.val, msg);
    }

    public static ApiException systemError(String msg) {
        return restException(ApiResultCodeEnum.SYSTEM_ERROR.val, msg);
    }

    private static ApiException restException(long code, String msg) {
        return ApiException.builder()
                .code(code)
                .msg(msg)
                .build();
    }
}
